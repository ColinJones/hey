require 'hey/version'

require 'base64'
require 'digest/sha1'
require 'securerandom'

require 'hey/constants'
require 'hey/processor'
require 'hey/screen'
require 'actions/kubernetes'
require 'actions/docker'

module Hey
  include Screen

  def initialize(options: {})
    @spin = true
    @cursor = TTY::Cursor
    @size = TTY::Screen.size
    @prompt = TTY::Prompt.new
    start
  end

  def start
    while @spin do
      choose_action
    end
  end
end

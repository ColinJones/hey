require 'tty-cursor'
require 'tty-screen'
require 'tty-prompt'

module Screen
  def choose_action
    user_input = @prompt.select("What do you want to do",
      ["Deploy Cluster", "Update Application", "Registry Actions", "Images Actions", "Exit"])
    case user_input
    when "Deploy Cluster"
      Kubernetes.deploy @options
    when "Update Application"
      Kubernetes.update @options
    when "Registry Actions"
      Docker.registry @options
    when "Images Actions"
      Docker.image @options
    when "Exit"
      @spin = false
    end
  end
end
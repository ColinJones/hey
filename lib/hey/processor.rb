require 'erb'
require 'json'
require 'rake'

class Processor
  attr_accessor :target_files
  attr_accessor :action

  class << self
    def process_files(type, filter)
      rake = Rake::Application.new
      Rake.application = rake
      rake.rake_require('processor', ["#{LIB_DIR}/rake"], []) if Rake::Task.tasks.empty?

      call = rake["process_files:#{type}:#{filter}"].invoke
      call[0].call
    end

    def build_file(template_path, key)
      ext = File.extname(template_path)
      generated_path = "%{#{key}/,#{BUILD}/#{key}/}X#{ext}"
      out_path = template_path.pathmap(generated_path)
      template = File.open(template_path).read
      generated_file = ERB.new(template).result

      setup_build_directory(out_path)
      out_file = File.new(out_path, 'w')
      out_file.write(generated_file)
      out_file.close
      out_file
    end

    def filter_files(directory, filter)
      Rake::FileList.new("#{directory}/**/*.#{filter}.*") do |files|
        files.exclude("**/~*")
        files.exclude(/\#{BUILD_DIR}\b/)
      end
    end

    def k8specs_filter(type)
      filter_files(K8SPECS_DIR, type)
    end

    def k8specs_process(file)
      build_file(file, K8SPECS)
    end

    def k8specs_output(filter)
      direxts = K8SPEC_EXT.join(',')
      Dir.glob("#{BUILD_DIR}/#{K8SPECS}/**/*.#{filter}.{#{direxts}}").select{ |e| File.file? e }
    end

    def dockerfiles_filter(type)
      filter_files(DOCKERFILES_DIR, type)
    end

    def dockerfiles_process(file)
      build_file(file, DOCKERFILES)
    end

    def dockerfiles_output(filter)
      direxts = DOCKERFILE_EXT.join(',')
      Dir.glob("#{BUILD_DIR}/#{DOCKERFILES}/**/*.#{filter}.{#{direxts}}").select{ |e| File.file? e }
    end

    def setup_stack
      Dir.mkdir(BASE_DIR) unless File.exist?(BASE_DIR)
      Dir.mkdir(K8SPECS_DIR) unless File.exist?(K8SPECS_DIR)
      Dir.mkdir(DOCKERFILES_DIR) unless File.exist?(DOCKERFILES_DIR)
    end

    def setup_build_directory(file_path)
      out_dir = Pathname.new(file_path)
      FileUtils.mkdir_p(out_dir.dirname) unless File.exist?(File.join(out_dir.dirname))
    end

    def cleanup_build_directories
      FileUtils.rm_rf(BUILD_DIR)
      FileUtils.rm_rf(IMAGES_DIR)
    end
  end
end

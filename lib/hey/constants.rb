require 'git'
require 'spicy-proton'

STACK = :stack
K8SPECS = :k8specs
DOCKERFILES = :dockerfiles
BUILD = :build
IMAGES = :images

K8SPEC_EXT = %w[yaml yml json].freeze
DOCKERFILE_EXT = ['Dockerfile'].freeze

PROCESSOR_KEYS = {
  :"#{K8SPECS}" => K8SPEC_EXT,
  :"#{DOCKERFILES}" =>  DOCKERFILE_EXT
}.freeze

DEPLOYMENT = :deployment
SERVICE = :service
INGRESS = :ingress
AUTOSCALER = :autoscaler
NAMESPACE = :namespace
SECRET = :secret

K8SPECS_FILTERS = [
  DEPLOYMENT,
  SERVICE,
  INGRESS,
  AUTOSCALER,
  NAMESPACE,
  SECRET
].freeze

DOCKERFILES_FILTERS = [
  :stubs,
  :server,
  :background,
  :job,
  :build
].freeze

FILTERS = {
  K8SPECS => K8SPECS_FILTERS,
  DOCKERFILES => DOCKERFILES_FILTERS
}

PWD = Dir.pwd.freeze
BASE_DIR = File.join(PWD, STACK.to_s).freeze
K8SPECS_DIR = File.join(BASE_DIR, K8SPECS.to_s).freeze
DOCKERFILES_DIR = File.join(BASE_DIR, DOCKERFILES.to_s).freeze
BUILD_DIR = File.join(BASE_DIR, BUILD.to_s).freeze

LIB_DIR = File.dirname(__FILE__) + '/..'

JOB_TIME_STAMP = Time.now.to_i
JOB_HASH = Digest::SHA1.hexdigest(JOB_TIME_STAMP.to_s)[0..10].freeze
JOB_NAME = Spicy::Proton.pair

git = Git.open(PWD)
HEAD_SHA = git.object('HEAD').sha

KUBE_CTL = 'kubectl'.freeze
DOCKER_CTL = 'docker'.freeze

if (KUBE_TOKEN = ENV['KUBE_TOKEN'])
  KUBE_EXE = "#{KUBE_CTL} --token=#{KUBE_TOKEN} -f".freeze
else
  KUBE_EXE = "#{KUBE_CTL} -f".freeze
end
namespace :process_files do
  namespace :k8specs do
    rule '.out'

    K8SPEC_EXT.each do |ext|
      rule ext => ->(file) do
        out_file = Processor.k8specs_process(file)
        out_file.path + '.out'
      end do |file|
      end
    end

    K8SPECS_FILTERS.each do |filter|
      task filter => Processor.k8specs_filter(filter) do |task|
        Processor.k8specs_output(filter)
      end
    end
  end

  namespace :dockerfiles do
    DOCKERFILE_EXT.each do |ext|
      rule ext => ->(file) do
        out_file = Processor.dockerfiles_process(file)
        out_file.path + '.out'
      end do |file|
      end
    end

    DOCKERFILES_FILTERS.each do |filter|
      task filter => Processor.dockerfiles_filter(filter) do |task|
        Processor.dockerfiles_output(filter)
      end
    end
  end
end

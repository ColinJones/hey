require 'kubeclient'

class Kubernetes
  include Screen
  attr_accessor :client

  def initialize(token: nil)
    @config = Kubeclient::Config.read(ENV['KUBECONFIG'] || "#{Dir.home}/.kube/config")
    puts "!!!!!!!"
    puts @config
    context = @config.context
    if(!token)
      @client = Kubeclient::Client.new(
        context.api_endpoint,
        'v1',
        ssl_options: context.ssl_options,
        auth_options: context.auth_options
      )
    else
      @client = Kubeclient::Client.new(
        context.api_endpoint, 'v1', auth_options: true
      )
    end
  end

  class << self
    def deploy(options)
      puts "Deploying"
      kube = new
      files = Processor.process_files(K8SPECS, DEPLOYMENT)
      files.each do |file|
        puts 'Applying :' + file
        kube.client.update_service Kubernetes.resource(file)
      end
    end

    def update(options)
      puts "Updating"
    end

    def resource(filename)
      text = File.open(filename).read
      case File.extname(filename)
      when '.yaml'
        parsedfiles = YAML.safe_load(text)
      when '.json'
        parsedfiles = JSON.parse(text)
      end
      Kubeclient::Resource.new(parsedfiles)
    end
  end
end
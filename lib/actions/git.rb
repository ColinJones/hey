require 'git'

class Git
  class << self
    def latest_commit_hash(branch: :master)
      g = Git.open(PWD)
      g.branches[branch].gcommit
    end

    def tag_release
      g = Git.open(PWD)
    end
  end
end
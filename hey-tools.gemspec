require_relative 'lib/hey/version'

Gem::Specification.new do |spec|
  spec.name          = "hey-tools"
  spec.version       = Hey::VERSION
  spec.authors       = ["Colin Jones"]
  spec.email         = ["colinstanfordjones@gmail.com"]

  spec.summary       = "I'm a task dispatcher. Give me an action and data."
  spec.description   = 'Generate, distribute and apply Dockerfiles and Kubernetes specs'
  spec.homepage      = 'https://gitlab.com/ColinJones/hey/-/wikis/Hey'
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/ColinJones/hey"
  spec.metadata["changelog_uri"] = "https://gitlab.com/ColinJones/hey/-/blob/master/CHANGLOG"

  spec.add_runtime_dependency 'docker-api'
  spec.add_runtime_dependency 'kubeclient'
  spec.add_runtime_dependency 'spicy-proton'
  spec.add_runtime_dependency 'git'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "bindir"
  spec.executables   = spec.files.grep(%r{^bindir/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end

RSpec.describe Hey do
  describe 'CONSTANTS' do
    it 'has STACK set' do
      expect(STACK).to be_truthy
    end

    it 'has K8SPECS set' do
      expect(K8SPECS).to be_truthy
    end

    it 'has DOCKERFILES  set' do
      expect(DOCKERFILES).to be_truthy
    end

    it 'has BUILD set' do
      expect(BUILD).to be_truthy
    end

    it 'has IMAGES set' do
      expect(IMAGES).to be_truthy
    end

    it 'has K8SPEC_EXT set' do
      expect(K8SPEC_EXT).to be_truthy
    end

    it 'has K8SPEC_EXT set' do
      expect(DOCKERFILE_EXT).to be_truthy
    end

    it 'has PROCESSOR_KEYS correctly set' do
      expect(PROCESSOR_KEYS).to match({
        :"#{K8SPECS}" => K8SPEC_EXT,
        :"#{DOCKERFILES}" =>  DOCKERFILE_EXT
      })
    end

    it 'has PWD correctly set' do
      expect(PWD).to match(Dir.pwd)
    end

    it 'has BASE_DIR correctly set' do
      expect(BASE_DIR)
        .to match(File.join(PWD, STACK.to_s))
    end

    it 'has K8SPECS_DIR correctly set' do
      expect(K8SPECS_DIR)
        .to match(File.join(BASE_DIR, K8SPECS.to_s))
    end

    it 'has DOCKERFILES_DIR correctly set' do
      expect(DOCKERFILES_DIR)
        .to match(File.join(BASE_DIR, DOCKERFILES.to_s))
    end

    it 'has BUILD_DIR correctly set' do
      expect(BUILD_DIR)
        .to match(File.join(BASE_DIR, BUILD.to_s))
    end

    it 'has BUILD_DIR correctly set' do
      expect(BUILD_DIR)
        .to be_truthy
    end

    it 'has JOB_HASH set' do
      expect(JOB_HASH).to be_truthy
    end

    it 'has JOB_NAME set' do
      expect(JOB_NAME).to be_truthy
    end

    it 'has HEAD_SHA set' do
      expect(HEAD_SHA).to be_truthy
    end

    it 'has HEAD_SHA set' do
      expect(KUBE_CTL)
        .to match('kubectl')
    end

    it 'has DOCKER_CTL correctly set' do
      expect(DOCKER_CTL)
        .to match('docker')
    end

    it 'has KUBE_EXE correctly set when token is present' do
      # expect(KUBE_EXE)
      #  .to match("#{KUBE_CTL} --token=#{KUBE_TOKEN} -f")
    end

    it 'has KUBE_EXE correctly set' do
      KUBE_EXE == "#{KUBE_CTL} -f"
    end
  end
end

FactoryBot.define do
  factory :k8specs, class: Processor do
    transient do
      ext { 'yaml' }
      file_filter { 'default' }
      yaml {
        <<~HEREDOC
          job_name:
            <%= JOB_NAME %>
        HEREDOC
      }

      yml {
        <<~HEREDOC
          job_name:
            <%= JOB_NAME %>
        HEREDOC
      }

      json {
        <<~HEREDOC
          { "job_name": "<%= JOB_NAME %>" }
        HEREDOC
      }

      control {
        "EMPTY"
      }
    end

    after(:build) do |k8spec, evaluator|
      randname = Spicy::Proton.pair
      ext = evaluator.ext
      file = "#{randname}.#{evaluator.file_filter}.#{evaluator.ext}"
      fullpath = File.join(K8SPECS_DIR, file)
      File.open(fullpath, 'w') {|f| f << evaluator.send(ext)}
      k8spec.target_files = [fullpath.delete_prefix("#{Dir.pwd}/")]
    end
  end
end
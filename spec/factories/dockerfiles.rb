FactoryBot.define do
  factory :dockerfiles, class: Processor do
    transient do
      ext { 'Dockerfile' }
      file_filter { 'default' }
      Dockerfile {
        <<~HEREDOC
          job_name:
            <%= JOB_NAME %>
        HEREDOC
      }

      control {
        'EMPTY'
      }
    end

    after(:build) do |dockerfile, evaluator|
      randname = Spicy::Proton.pair
      ext = evaluator.ext
      file = "#{randname}.#{evaluator.file_filter}.#{evaluator.ext}"
      fullpath = File.join(DOCKERFILES_DIR, file)
      File.open(fullpath, 'w') {|f| f << evaluator.send(ext)}
      dockerfile.target_files = [fullpath.delete_prefix("#{Dir.pwd}/")]
    end
  end
end

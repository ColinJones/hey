gem 'spicy-proton'
FactoryBot.define do
  factory :processor do
    transient do
      initialize_files { Hash.new }
    end

    target_files { [] }
    action { }

    after(:build) do |processor, evaluator|
      Processor.setup_stack

      evaluator.initialize_files.keys.each do |key|
        evaluator.initialize_files[key].each do |filter, files|
          process_list = files.is_a?(Array) ? files : [files]
          process_list.each do |f|
            mock_file = build(key, file_filter: filter, ext: f[:ext])
            processor.target_files += mock_file.target_files unless f[:control]
          end
        end
      end
    end
  end
end

shared_context "rake_processor" do
  let(:rake)      { Rake::Application.new }
  let(:task_name) { self.class.top_level_description }
  let(:task_path) { "lib/rake/processor" }

  before(:each) do
    Rake.application = rake
    Rake.application.rake_require(task_path, [Dir.pwd], task_path) if Rake::Task.tasks.empty?
    allow(Processor)
      .to receive(:build_file).and_call_original
  end
end
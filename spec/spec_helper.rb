require 'hey'
require 'factory_bot'

require 'simplecov'
SimpleCov.start

RSpec.configure do |config|
  Dir["./spec/support/**/*.rb"].sort.each { |f| require f}
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = 'spec/.rspec_status'

  config.expose_dsl_globally = true

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.order = :random

  config.include FactoryBot::Syntax::Methods

  config.before(:suite) do
    FactoryBot.find_definitions
  end
end

RSpec.describe Processor do
  describe 'Rake:Task' do
    after do
      FileUtils.rm_rf(BASE_DIR)
    end

    PROCESSOR_KEYS.each do |action, exts|
      describe "#{action}" do
        case action
        when K8SPECS
          filters = K8SPECS_FILTERS
          file_exts = K8SPEC_EXT
        when DOCKERFILES
          filters = DOCKERFILES_FILTERS
          file_exts = DOCKERFILE_EXT
        end

        filters.each do |filter|
          describe "task[process_files:#{action}:#{filter}]" do
            before do
              @mockfiles = {
                action => {
                  filter => file_exts.map{ |t| { ext: t, control: false } }
                }
              }

              @control_files = {
                action => {
                  control: file_exts.map{ |t| { ext: t, control: false } }
                }
              }

              @processor = build(:processor, initialize_files: @mockfiles)
              @control_processor = build(:processor, initialize_files: @control_files)
              @call_count = @processor.target_files.count
            end
            include_context 'rake_processor'

            let(:run_rake_task) {
              Rake::Task["process_files:#{action}:#{filter}"].reenable
              Rake::Task["process_files:#{action}:#{filter}"].invoke[0]
            }

            it "Should call Processor.build_file the correct amount" do
              expect(Processor)
                .to receive(:build_file)
                .exactly(@call_count).times
              run_rake_task
            end

            it 'Should not process control files' do
              @processor.target_files.each do |processed_file|
                expect(Processor)
                  .not_to receive(:build_file)
                  .with(processed_file, action)
              end
              run_rake_task
            end
          end
        end

        exts.each do |ext|
          describe "rule[.#{ext}]" do
            let :mockfiles do
              { action => {
                  default: [
                    { ext: ext, control: false },
                    { ext: ext, control: false },
                    { ext: ext, control: false }
                  ]
                }
              }
            end

            let :control_files do
              { action => {
                  default: [
                    { ext: 'control', control: false},
                    { ext: 'control', control: false}
                  ]
                }
              }
            end

            let :run_rake_task do
              rake_path = "stack/**/*"
              [*Rake::FileList.new(rake_path)].each do |path|
                Rake::Task[path].reenable
              end
            end

            let(:processor) { build(:processor, initialize_files: mockfiles) }
            let(:control_processor) { build(:processor, initialize_files: control_files) }
            let(:call_count) { processor.target_files.count }
            include_context 'rake_processor'

            it "Should process .#{ext}" do
              processor.target_files.each do |processed_file|
                expect(Processor)
                  .to receive(:build_file)
                  .with(processed_file, action).once
              end
              run_rake_task
            end

            it 'Should not process .control' do
              control_processor.target_files.each do |processed_file|
                expect(Processor)
                  .not_to receive(:build_file)
                  .with(processed_file, action)
              end
              run_rake_task
            end

            it 'Should call Processor.build_file the correct amount' do
              expect(Processor)
                .to receive(:build_file)
                .exactly(call_count).times
              run_rake_task
            end
          end
        end
      end
    end
  end
end

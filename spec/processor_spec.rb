require 'yaml'
require 'json'

RSpec.describe Processor do
  after do
    FileUtils.rm_rf(BASE_DIR)
  end

  describe '.process_files' do
    FILTERS.each do |type, filters|
      describe "#{type}" do
        filters.each do |filter|
          before do
            processor
          end

          let(:mockfiles) do
            {
              type => {
                filter => [
                  { ext: PROCESSOR_KEYS[type][0], control: false },
                  { ext: PROCESSOR_KEYS[type][0], control: false },
                  { ext: PROCESSOR_KEYS[type][0], control: false }
                ],
                default: [
                  { ext: PROCESSOR_KEYS[type][0], control: true }
                ]
              }
            }
          end

          let(:processor) { build(:processor, initialize_files: mockfiles) }
          let(:process_files) { Processor.process_files(type, filter) }

          it "Should return the correct list on filter - #{filter}" do
            expect(process_files.count).to eq(processor.target_files.count)
          end

          it "Should build valid files on filter - #{type} #{filter}" do
            process_files.each do |file|
              expect(File.exist?(file)).not_to be_nil
            end
          end
        end
      end
    end
  end

  PROCESSOR_KEYS.each do |action, exts|
    describe ".build_file for #{action}" do
      exts.each do |ext|
        describe "file.#{ext}" do
          let :mockfiles do
            { action => {
              default: [
                  { ext: ext, control: false }
                ]
              }
            }
          end

          let(:processor) { build(:processor, initialize_files: mockfiles) }
          let(:file_path) { File.path(processor.target_files[0]) }

          let :generated_file  do
            Processor.build_file(file_path, action)
          end

          let :file_token  do
            { "job_name" => JOB_NAME }
          end

          it "should generate built .#{ext}" do
            expect(File.exist?(generated_file.path)).to be_truthy
          end

          it "should generate valid .#{ext}" do
            path = generated_file
            file = File.open(path).read
            case ext
            when /yaml|yml|Dockerfile/
              job_name = YAML.load(file)
            when /json/
              job_name = JSON.parse(file)
            end
            expect(job_name).to eq(file_token)
          end
        end
      end
    end
  end
end
